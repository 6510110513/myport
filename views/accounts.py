from flask import Blueprint, render_template, request, redirect, url_for
from flask_login import login_required, login_user, logout_user , current_user
from werkzeug.utils import secure_filename
from werkzeug.security import generate_password_hash , check_password_hash
import os

from forms import accountslog as accounts_form
from forms import registerweb as regester_form
from forms import addport as addport_form
from forms import editport as editport_form
from models import users
from models.database import user
from models.database import port

UPLOAD_FOLDERMy = 'static/img'
UPLOAD_FOLDERPort = 'static/img_port'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}


bp = Blueprint("user",__name__,"/users")

user_login = user()

@bp.route("/register")
def register():
    user_db = user()
    try:
        current_user_in = user_db.find_one({"id":current_user.id})
        current_user_in = current_user_in["name"]
    except:current_user_in = "please login"
    form = regester_form.RegisterForm()
    return render_template("/users/register.html",form=form,current_user_in=current_user_in)

@bp.route("/do-register" , methods = ["POST"])
def do_register():
    user_db = user()
    try:
        current_user_in = user_db.find_one({"id":current_user.id})
        current_user_in = current_user_in["name"]
    except:current_user_in = "please login"
    form = regester_form.RegisterForm()
    userlog = user()
    if userlog.find_one({"email":form.email.data}):
        return redirect(url_for("user.register", message="email is used"))
    if not form.validate_on_submit():
        return redirect(url_for("user.register", **form.errors))
    password = generate_password_hash(form.password.data)
    userlog.insert_one({"name":form.name.data,"email":form.email.data,"password":password,"id":form.name.data+form.email.data})
    return render_template("/users/regispass.html",current_user_in=current_user_in)

@bp.route("/login")
def login():
    user_db = user()
    try:
        current_user_in = user_db.find_one({"id":current_user.id})
        current_user_in = current_user_in["name"]
    except:current_user_in = "please login"
    form = accounts_form.LoginForm()
    return render_template("/users/login.html", form=form,current_user_in=current_user_in)

@bp.route("/do-login", methods=["POST"])
def do_login():
    
    # email = request.form.get("email")
    # password = request.form.get("password")

    # print("-->", email, password)
    # print(request.form)


    form = accounts_form.LoginForm()

    if not form.validate_on_submit():
        return redirect(url_for("user.login", **form.errors))

    user_data = user_login.find_one({"email":form.email.data})

    if not user_data:
        return redirect(url_for("user.login", message = "invalid email"))

    if not check_password_hash(user_data["password"],form.password.data):
        return redirect(url_for("user.login", message="invalid password"))

    #user_data = None
    #for id, u in database["users"].items():
    #    if u.get("email") == form.email.data:
    #        user_data = u

    #if not (u and form.password.data == u.get("password")):
    #    return redirect(url_for("user.login", message="invalid login"))

    user = users.User(**user_data)
    login_user(user)

    print(user)
    print(user.__dict__)

    return redirect(url_for("user.list_edit"))

@bp.route("/logout")
def logout():
    logout_user()
    return redirect("/")

@bp.route("/add")
@login_required
def add():
    user_db = user()
    try:
        current_user_in = user_db.find_one({"id":current_user.id})
        current_user_in = current_user_in["name"]
    except:current_user_in = "please login"
    form = addport_form.AddForm()
    return render_template("/users/add.html",form = form,current_user_in=current_user_in)

@bp.route("/do-add" , methods = ["POST"])
@login_required
def do_add():
    form = addport_form.AddForm()

    #bp.config['UPLOAD_FOLDER'] = UPLOAD_FOLDERMy
    profile = request.files['photomy']
    path_save_myphoto = ''
    if profile:
        profile_name = secure_filename(profile.filename)
        path_save_myphoto = os.path.join(UPLOAD_FOLDERMy, profile_name)
        profile.save(path_save_myphoto)

    #bp.config['UPLOAD_FOLDER'] = UPLOAD_FOLDERPort
    profile = request.files['photoperformance']
    path_save_profile = ""
    if profile:
        profile_name = secure_filename(profile.filename)
        path_save_profile = os.path.join(UPLOAD_FOLDERPort, profile_name)
        profile.save(path_save_profile)

    all_port = port()
    list_myport = all_port.find({"user_id":current_user.id})
    print("port_id ==> ",list_myport)
    list_myport = list(list_myport)
    if  len(list_myport) != 0:
        number_myport = list_myport[-1]
        number_myport = number_myport["port_id"] + 1
    else:
        number_myport = 0

    add_port = port()
    add_port.insert_one({   
                            "photomy":path_save_myphoto,
                            "fname":form.fname.data,
                            "lname":form.lname.data,
                            "email":form.email.data,
                            "road":form.road.data,
                            "tambon":form.tambon.data,
                            "amphor":form.amphor.data,
                            "city":form.city.data,
                            "zip_code":form.zip_code.data,
                            "tel":form.tel.data,
                            "skill":form.skill.data,
                            "performance":form.performance.data,
                            "photoperformance":path_save_profile,
                            "linkedin":form.linkedin.data,
                            "github":form.github.data,
                            "twitter":form.twitter.data,
                            "facebook":form.facebook.data,
                            "user_id":current_user.id,
                            "port_id":number_myport,
                            "user_port_id":current_user.id+str(number_myport)
                            
                        })
    return redirect(url_for('user.list_edit'))

@bp.route("/edit")
@login_required
def edit():
    user_db = user()
    try:
        current_user_in = user_db.find_one({"id":current_user.id})
        current_user_in = current_user_in["name"]
    except:current_user_in = "please login"
    return render_template("/users/edit.html",current_user_in=current_user_in)

@bp.route("/list_edit")
@login_required
def list_edit():
    user_db = user()
    try:
        current_user_in = user_db.find_one({"id":current_user.id})
        current_user_in = current_user_in["name"]
    except:current_user_in = "please login"
    load_users = current_user.id
    print("id => ",load_users)
    all_port = port()
    list_port = all_port.find({"user_id":load_users})
    list_port = list(list_port)
    l = len(list_port)
    data = [list_port[i:i+3] for i in [3*(ip) for ip in range((l//3))]]
    ll = len(data)
    if not ll*3 == l:
        data.append(list_port[ll*3:])
    #data.append(l%3)
    return render_template("/users/listmyport.html",datas = data,current_user_in=current_user_in)

@bp.route("/form-edit/<port_ids>" , methods = ["GET","POST"])
@login_required
def form_edit(port_ids):
    user_db = user()
    try:
        current_user_in = user_db.find_one({"id":current_user.id})
        current_user_in = current_user_in["name"]
    except:current_user_in = "please login"
    all_port = port()
    form = editport_form.EditForm()
    user_port_id = current_user.id + port_ids
    data_myport = all_port.find_one({"user_port_id":user_port_id})
    return render_template("/users/edit.html",form = form,datas = data_myport,current_user_in=current_user_in)

@bp.route("/do-edit" , methods = ["POST"])
@login_required
def do_edit():
    form = editport_form.EditForm()
    user_port_id = form.user_port_id.data
    print("===>",user_port_id)

    all_port = port()
    data_myport = all_port.find_one({"user_port_id":user_port_id})

    #bp.config['UPLOAD_FOLDER'] = UPLOAD_FOLDERMy
    
    if request.method == 'POST':
        profile = request.files['photomy']
    if profile:
        profile_name = secure_filename(profile.filename)
        path_save_myphoto = os.path.join(UPLOAD_FOLDERMy, profile_name)
        profile.save(path_save_myphoto)
        path_photomy = path_save_myphoto
    else:
        path_photomy = data_myport["photomy"]

    #bp.config['UPLOAD_FOLDER'] = UPLOAD_FOLDERPort
    performance = request.files['photoperformance']
    if performance:
        profile_name = secure_filename(performance.filename)
        path_save_profile = os.path.join(UPLOAD_FOLDERPort, profile_name)
        performance.save(path_save_profile)
        path_photoperformance = path_save_profile
    else:
        path_photoperformance = data_myport["photoperformance"]

    add_port = port()
    myquery = {"user_port_id":user_port_id}
    data_update = { "$set":{
                            "photomy":path_photomy,
                            "fname":form.fname.data,
                            "lname":form.lname.data,
                            "email":form.email.data,
                            "road":form.road.data,
                            "tambon":form.tambon.data,
                            "amphor":form.amphor.data,
                            "city":form.city.data,
                            "zip_code":form.zip_code.data,
                            "tel":form.tel.data,
                            "skill":form.skill.data,
                            "performance":form.performance.data,
                            "photoperformance":path_photoperformance,
                            "linkedin":form.linkedin.data,
                            "github":form.github.data,
                            "twitter":form.twitter.data,
                            "facebook":form.facebook.data,
                            "user_id":current_user.id,
                            "port_id":data_myport["port_id"],
                            "user_port_id":user_port_id
                    }    }

    add_port.update_one(myquery,data_update)
    return redirect(url_for('user.list_edit'))

@bp.route("/port/<user_port_id>" ,methods = ["GET"])
def view_port(user_port_id):
    user_db = user()
    try:
        current_user_in = user_db.find_one({"id":current_user.id})
        current_user_in = current_user_in["name"]
    except:current_user_in = "please login"
    open_data = port()
    data_port = open_data.find_one({"user_port_id":user_port_id})
    return render_template("/main/port.html",datas = data_port,current_user_in=current_user_in)

@bp.route("/delete_file/<user_port_id>",methods = ["GET","POST"])
@login_required
def delete(user_port_id):
    my_port_data = port()
    my_port_data.delete_one({"user_port_id":user_port_id})
    return redirect(url_for('user.list_edit'))
