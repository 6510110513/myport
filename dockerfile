# Use an official debian sid as a parent image
FROM python:3-alpine
# Update and Upgrade system then install python3
#RUN apt update && apt upgrade -y
#RUN apt install -y python3 python3-dev python3-pip
# Set the working directory to /app
WORKDIR /app
# Copy the current directory contents into the container at /app
COPY . /app
# Install any needed packages specified in requirements.txt
RUN pip3 install --trusted-host pypi.python.org -r requirement.txt
# Make port 8000 available to the world outside this container
EXPOSE 8000
# Define environment variable, change it to your infomation
ENV WORLD Docker
ENV STUDENTID "STUDENT_ID"
ENV NAME "STUDENT NAME"
# Run app.py when the container launches
CMD ["python3", "main.py"]