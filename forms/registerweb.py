from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import validators


class RegisterForm(FlaskForm):
    name = StringField(
        "name", validators=[validators.DataRequired()]
    )
    email = StringField(
        "name", validators=[validators.DataRequired()]
    )
    password = StringField(
        "password",
        validators=[
            validators.DataRequired(),
            validators.Length(min=4, message="รหัสผ่านอย่างน้อย 4 ตัว")
        ],
    )
