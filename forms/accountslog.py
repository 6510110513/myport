from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import validators


class LoginForm(FlaskForm):
    email = StringField(
        "name", validators=[validators.DataRequired()]
    )
    password = StringField(
        "password",
        validators=[
            validators.DataRequired()
        ],
    )
