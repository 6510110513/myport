from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import validators


class AddForm(FlaskForm):
    photomy = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    fname = StringField(
        "name",
        validators=[validators.DataRequired(),validators.Length(min=3, message="มากกว่า 3")]
    )
    lname = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    email = StringField(
        "email", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    road = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    tambon = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    amphor = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    city = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    zip_code = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    tel = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    skill = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    performance = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    photoperformance = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    linkedin = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    github = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    twitter = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    facebook = StringField(
        "name", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
