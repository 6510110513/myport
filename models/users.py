from flask_login import UserMixin

class User(UserMixin):
    def __init__(self, **kwargs):
        self.email = kwargs.get("email")
        self.password = kwargs.get("password")
        self.id = kwargs.get("id")
