from flask import Flask , redirect ,render_template ,url_for
from flask_login import LoginManager
from models.database import port
from flask_login import current_user , login_required

from views import accounts
from models import users
from models.database import user


user_db = user()

login_manager = LoginManager()
login_manager.login_view = "accounts.login"

@login_manager.unauthorized_handler
def unauthorized():
    # do stuff
    # return "You don't have permission"
    print("You don't have permission")
    return redirect(url_for("user.login"))

app = Flask(__name__)


@login_manager.user_loader
def load_user(user_id):
    user_data = user_db.find_one({"id":user_id})
    print(user_id)
    print("->", user_data)
    if user_data:
        return users.User(**user_data)

    return None

@app.route("/")
def home():
    user_db = user()
    try:
        current_user_in = user_db.find_one({"id":current_user.id})
        current_user_in = current_user_in["name"]
    except:current_user_in = "please login"
    all_port = port()
    list_port = all_port.find({})
    list_port = list(list_port)
    l = len(list_port)
    data = [list_port[i:i+3] for i in [3*(ip) for ip in range((l//3))]]
    ll = len(data)
    if not ll*3 == l:
        data.append(list_port[ll*3:])
    #data.append(l%3)
    return render_template("/main/index.html",datas=data,current_user_in=current_user_in)

if __name__ == "__main__":
    app.secret_key = "123456789"
    login_manager.init_app(app)
    app.register_blueprint(accounts.bp)
    app.run(host="0.0.0.0",port=8000,debug=True)